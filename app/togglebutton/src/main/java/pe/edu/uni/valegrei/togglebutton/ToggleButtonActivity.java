package pe.edu.uni.valegrei.togglebutton;

import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ToggleButton;

import androidx.appcompat.app.AppCompatActivity;

public class ToggleButtonActivity extends AppCompatActivity {

    ToggleButton toggleButton;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toggle_button);

        toggleButton = findViewById(R.id.toggle_button);
        imageView = findViewById(R.id.image_view);

        toggleButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(isChecked){
                imageView.setVisibility(View.INVISIBLE);
            }else{
                imageView.setVisibility(View.VISIBLE);
            }
        });
    }
}