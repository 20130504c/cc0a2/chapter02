package pe.edu.uni.valegrei.sharedpreference;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class SharedPreferenceActivity extends AppCompatActivity {
    EditText editTextName, editTextMessage;
    Button button;
    CheckBox checkBox;

    int counter = 0;
    SharedPreferences sharedPreferences;
    String name, message;
    Boolean isChecked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_preference);

        editTextName = findViewById(R.id.edit_text_name);
        editTextMessage = findViewById(R.id.edit_text_message);
        button = findViewById(R.id.button);
        checkBox = findViewById(R.id.check_box_recordar);

        button.setOnClickListener(v -> {
            counter++;
            button.setText(String.valueOf(counter));
        });
        retrieveData();
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }

    private void saveData() {
        sharedPreferences = getSharedPreferences("saveData", Context.MODE_PRIVATE);
        name = editTextName.getText().toString();
        message = editTextMessage.getText().toString();
        isChecked = checkBox.isChecked();

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("key name", name);
        editor.putString("key message", message);
        editor.putInt("key counter", counter);
        editor.putBoolean("key remember", isChecked);
        editor.apply();

        Toast.makeText(this, R.string.text_save, Toast.LENGTH_SHORT).show();
    }

    private void retrieveData() {
        sharedPreferences = getSharedPreferences("saveData", Context.MODE_PRIVATE);
        name = sharedPreferences.getString("key name", null);
        message = sharedPreferences.getString("key message", null);
        counter = sharedPreferences.getInt("key counter", 0);
        isChecked = sharedPreferences.getBoolean("key remember", false);

        editTextName.setText(name);
        editTextMessage.setText(message);
        button.setText(String.valueOf(counter));
        checkBox.setChecked(isChecked);
    }

}