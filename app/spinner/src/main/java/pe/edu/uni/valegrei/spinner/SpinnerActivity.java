package pe.edu.uni.valegrei.spinner;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

public class SpinnerActivity extends AppCompatActivity {
    ImageView imageViewLogo;
    Spinner spinnerLogo;

    ArrayAdapter<CharSequence> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner);

        imageViewLogo = findViewById(R.id.image_view_logo);
        spinnerLogo = findViewById(R.id.spinner_logo);

        adapter = ArrayAdapter.createFromResource(this, R.array.logos, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerLogo.setAdapter(adapter);

        spinnerLogo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        imageViewLogo.setImageResource(R.drawable.d);
                        return;
                    case 1:
                        imageViewLogo.setImageResource(R.drawable.a);
                        return;
                    case 2:
                        imageViewLogo.setImageResource(R.drawable.c);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}