package pe.edu.uni.valegrei.button;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ButtonActivity extends AppCompatActivity {

    Button button1, button2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_button);

        button1 = findViewById(R.id.button_1);
        button2 = findViewById(R.id.button_2);

        button1.setOnClickListener(v -> {
            button1.setBackgroundColor(Color.RED);
            button2.setVisibility(View.VISIBLE);
        });
    }
}