package pe.edu.uni.valegrei.intent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {
    TextView textViewText, textViewNumber;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        textViewText = findViewById(R.id.textview_text);
        textViewNumber = findViewById(R.id.textview_number);

        Intent intent = getIntent();

        String text = intent.getStringExtra("TEXT");
        int number = intent.getIntExtra("NUMBER",0);

        textViewText.setText(text);
        textViewNumber.setText(String.valueOf(number));
    }
}