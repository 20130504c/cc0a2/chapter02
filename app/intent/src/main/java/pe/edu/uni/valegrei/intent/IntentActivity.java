package pe.edu.uni.valegrei.intent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

public class IntentActivity extends AppCompatActivity {
    Button button;
    EditText editText, editNumber;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intent);

        editText = findViewById(R.id.edit_text);
        editNumber = findViewById(R.id.edit_number);
        button = findViewById(R.id.button_intent);
        button.setOnClickListener(v -> {
            String sText = editText.getText().toString();
            String sNumber = editNumber.getText().toString();

            Intent intent = new Intent(IntentActivity.this, SecondActivity.class);
            intent.putExtra("TEXT",sText);
            if(!sNumber.trim().equals("")){
                int number = Integer.parseInt(sNumber);
                intent.putExtra("NUMBER",number);
            }

            startActivity(intent);
            //finish();
        });
    }
}