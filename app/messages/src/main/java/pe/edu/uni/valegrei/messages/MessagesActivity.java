package pe.edu.uni.valegrei.messages;

import android.os.Bundle;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;

public class MessagesActivity extends AppCompatActivity {
    Button buttonToast, buttonSnackBar, buttonDialog;
    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);

        buttonToast = findViewById(R.id.button_message_1);
        buttonSnackBar = findViewById(R.id.button_message_2);
        buttonDialog = findViewById(R.id.button_message_3);
        linearLayout = findViewById(R.id.linearLayout);

        buttonToast.setOnClickListener(v ->
                Toast.makeText(getApplicationContext(), R.string.msg_toast, Toast.LENGTH_SHORT).show()
        );

        buttonSnackBar.setOnClickListener(v ->
                Snackbar.make(linearLayout, R.string.msg_snackbar, Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.button_snackbar_text, v1 -> {

                        }).show()
        );

        buttonDialog.setOnClickListener(v -> {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder
                    .setTitle(R.string.dialog_title)
                    .setMessage(R.string.dialog_msg)
                    .setPositiveButton(R.string.yes, (dialog, which) -> {

                    })
                    .setNegativeButton(R.string.no, (dialog, which) -> dialog.cancel()).show();
        });
    }
}