package pe.edu.uni.valegrei.barcodescanner;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bosphere.filelogger.FL;
import com.google.mlkit.common.MlKitException;
import com.google.mlkit.vision.barcode.common.Barcode;
import com.google.mlkit.vision.codescanner.GmsBarcodeScanner;
import com.google.mlkit.vision.codescanner.GmsBarcodeScannerOptions;
import com.google.mlkit.vision.codescanner.GmsBarcodeScanning;

public class ScannerActivity extends AppCompatActivity {
    private static final String TAG = ScannerActivity.class.getSimpleName();

    TextView tvScanner;
    Button btnScanner;
    GmsBarcodeScanner scanner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FL.i(TAG, "onCreate");
        setContentView(R.layout.activity_scanner);

        tvScanner = findViewById(R.id.tv_scanner);
        btnScanner = findViewById(R.id.btn_scanner);

        initScanner();

        btnScanner.setOnClickListener(v -> scan());
    }

    private void initScanner() {
        GmsBarcodeScannerOptions options = new GmsBarcodeScannerOptions.Builder()
                .setBarcodeFormats(
                        Barcode.FORMAT_QR_CODE,
                        Barcode.FORMAT_AZTEC)
                .build();
        // With a configured options
        scanner = GmsBarcodeScanning.getClient(this, options);
    }

    private void scan() {
        FL.i(TAG, "this is a debug message");
        scanner
                .startScan()
                .addOnSuccessListener(barcode -> {
                    // Task completed successfully
                    FL.i(TAG,"addOnSuccessListener");
                    tvScanner.setText(getSuccessFullMessage(barcode));
                    urlHandler(barcode);
                })
                .addOnCanceledListener(() -> {
                    // Task canceled
                    FL.i(TAG,"addOnCanceledListener");
                    tvScanner.setText(getString(R.string.barcode_cancel));
                })
                .addOnFailureListener(e -> {
                    // Task failed with an exception
                    FL.i(TAG,"Error: %s",e);
                    if (e instanceof MlKitException)
                        tvScanner.setText(getFailureExceptionMessage((MlKitException) e));
                    else
                        tvScanner.setText(getString(R.string.error_default, e));
                });
    }

    private String getSuccessFullMessage(Barcode barcode) {
        return getString(R.string.barcode_result,
                barcode.getRawValue(),
                barcode.getFormat(),
                barcode.getValueType());
    }

    private void urlHandler(Barcode barcode) {
        if (barcode.getValueType() == Barcode.TYPE_URL) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri uri = Uri.parse(barcode.getUrl().getUrl());
            intent.setData(uri);
            startActivity(intent);
        }
    }

    private String getFailureExceptionMessage(MlKitException e) {
        switch (e.getErrorCode()) {
            case MlKitException.CODE_SCANNER_CANCELLED:
                return getString(R.string.error_code_scanner_cancel);
            case MlKitException.UNKNOWN:
                return getString(R.string.error_code_unknow);
            default:
                return getString(R.string.error_default, e);
        }
    }
}