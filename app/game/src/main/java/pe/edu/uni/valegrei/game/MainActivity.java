package pe.edu.uni.valegrei.game;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.RadioButton;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;

public class MainActivity extends AppCompatActivity {
    RadioButton radioButtonTwo, radioButtonThree, radioButtonFour;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        radioButtonTwo = findViewById(R.id.radio_button_two);
        radioButtonThree = findViewById(R.id.radio_button_three);
        radioButtonFour = findViewById(R.id.radio_button_four);
        button = findViewById(R.id.button_start);

        button.setOnClickListener(v -> {
            if(!radioButtonTwo.isChecked() && !radioButtonThree.isChecked() && !radioButtonFour.isChecked()){
                Snackbar.make(v,R.string.validar_msg,Snackbar.LENGTH_LONG).show();
                return;
            }
            Intent intent = new Intent(MainActivity.this, GameActivity.class);
            if(radioButtonTwo.isChecked()){
                intent.putExtra("TWO",true);
            }
            if(radioButtonThree.isChecked()){
                intent.putExtra("THREE",true);
            }
            if(radioButtonFour.isChecked()){
                intent.putExtra("FOUR",true);
            }

            startActivity(intent);
        });
    }
}